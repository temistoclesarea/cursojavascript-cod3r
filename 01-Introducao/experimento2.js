
function esperarPor(tempo) {
  const futuro = Date.now() + tempo
  while(Date.now() < futuro) {}
}

setInterval(() => console.log('Exec #01'), 400)
setTimeout(() => {
  esperarPor(3000)
  console.log('Exec #02')
}, 300)

esperarPor(5000)
console.log('Fim!')

// Sempre vai ter só um processo por vez
// Enquanto ele estiver ocupando a stack, nada pode ser processado no event queue
// Vai ter sempre um sequenciamento lógico ao executar cada pendencia entre o event queue e stack
// O event queue não executa nada dentro dele, ele tem que mandar pra stack para as coisas serem executadas
