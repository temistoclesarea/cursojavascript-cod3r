// Arrow Function
const felizNatal = () => console.log('Feliz Natal!!!') // o return é undefined
felizNatal()

// arrow funcions sempre vai estar usando functions expression ao inves de function declaration
// se tiver um unico paramentro, pode remover os parenteses da arrow function
const saudacao = nome => `Fala ${nome} !?! ${2 * 2}` // backtick `
console.log(saudacao('Maria'))

const teste = function(val) {
  console.log('Teste com function')
}
teste()

const somar = (...numeros) => {
  console.log(Array.isArray(numeros))
  console.log(typeof numeros)
  let total = 0
  for(let n of numeros) {
    total += n
  }
  return total // se tiver corpo, precisa colocar o return
}
// console.log(somar([1,2,3,4,5])) // sem o operador rest
console.log(somar(1,2,3)) // com o operador rest

/* function potencia(base) {
  return function(exp) {
    return Math.pow(base, exp)
  }
} */
// convertido em arrow function
const potencia = base => exp => Math.pow(base, exp)
console.log(potencia(2)(4))

// this
Array.prototype.log = function() {
  //console.log('Opaa!!')
  console.log(this)
}

Array.prototype.ultimo = function() {
  console.log(this[this.length - 1])
}
// obs. o this não funciona em arrow function, da undefined
// this tem mais haver com OO do que programação funcional
// this tem haver com contexto lexo
Array.prototype.primeiro = function () {
  console.log(this[0])
}

const numeros = [-1,1,2,3,10]
numeros.log()
numeros.ultimo()
numeros.primeiro()

const subtrair = (a,b) => a - b
console.log(subtrair(10,3))
