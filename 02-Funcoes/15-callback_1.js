

const somarNoTerminal = (a, b) => console.log(a + b)
const subtrairNoTerminal = (a, b) => console.log(a - b)
const subtrair = (a, b) => a - b
const exec = (fn, a, b) => fn(a, b)

exec(somarNoTerminal, 56, 38)
exec(subtrairNoTerminal, 182, 27)

// callback é passar uma função como parametro pra outra função e ela vai ser chamada de volta
// conceito de programação reativa - quando acontecer um evento, eu chamo a função de volta
// aconteceu algum evento e essa função é chamada de volta

const r = exec(subtrair, 150, 50)

console.log(r)

// setInterval - executa de forma repetida, similar a um temporizador

setInterval(() => console.log('Exec...'), 5000)

// Associado a acontecer um evento, leitura de arquivo, requisição e obteve resposta
// callback é o mesmo mecanismo de passar uma função dentro de outro função
// callback é fundamental
