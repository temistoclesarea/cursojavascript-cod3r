
const nums = [1, 2, 3, 4, 5]
// map recebe uma função com até 3 valores (n = valor, i = indice, a = array)
// dentro do map tem um laço for interno
const dobro = (n, i, a) => n * 2 + i + a.length

console.log(nums.map(dobro))


const nomes = ['Ana', 'Bia', 'Gui', 'Lia', 'Rafa']

// da pra acessar a primeira letra pelo [] em uma string
const primeiraLetra = texto => texto[0]
const letras = nomes.map(primeiraLetra)

console.log(nomes, letras)
