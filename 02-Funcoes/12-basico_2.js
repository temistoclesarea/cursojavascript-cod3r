// Básico sobre Função #02
// passar função como paramento em outra função
// programação funcional e reativa se baseia nesses principios

function bomDia() {
  console.log('Bom dia!')
}

const boaTarde = function () { // função anônima
  console.log('Boa tarde!')
}

// 1) Passar uma função como param pra outra função
function executarQualquerCoisa(fn) {
  console.log(typeof fn)
  if (typeof fn === 'function') {
    fn()
  }

}

executarQualquerCoisa(3)
executarQualquerCoisa(bomDia)
executarQualquerCoisa(boaTarde)

// o fato de existir essa possibilidade, abre um leque de possibilidades muito grande

console.log(1 == '1') // compara somente os valores, independente do tipo

console.log(1 === '1') // comparação extrita - compara os valores e tambem os tipos

// 2) Retornar uma função a partir de uma outra função
// obs. variaveis não podem começar com numeros em javascript, causa um erro
// ele esta mostrando o mecanismo, não esta mostrando o uso na pratica no momento
function potencia(base) {
  return function(exp) {
    return Math.pow(base, exp)
  }
}

const potenciaDe2 = potencia(2)
console.log(potenciaDe2(8))

// chamando tudo junto
const pot34 = potencia(3)(4)
console.log(pot34)
