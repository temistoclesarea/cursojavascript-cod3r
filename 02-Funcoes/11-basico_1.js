// Básico sobre Função #01

// Diferença entre Funciton Declaration e Function Expression
// Como criar uma declaração de função e uma expressão de função

let a = 4
console.log(a)

// Function declaration
function bomDia() {
  console.log('Bom Dia!')
}

bomDia()

// Function expression
const boaTarde = function () {
  console.log('Boa Tarde!')
}

boaTarde()

let x = boaTarde() // undefined
console.log(x)

// Javascript é multiparadigma - da pra implementar programação procedural, orientada a objeto e funcional

function somar(a = 0, b = 0) {
  return a + b
}

let resultado = somar(3, 4)
console.log(resultado)

resultado = somar(4, 4, 5, 6, 9) // flexibilidade dos parametros
console.log(resultado)

resultado = somar(3)
console.log(resultado)

// NaN = Not a Number

resultado = somar()
console.log(resultado)
