// Desafio
console.log('Desafio')



// 1) somar (3)(4)(5)

function somar(a) {
  return function(b) {
    return function(c) {
      return a + b + c
    }
  }
}

const total = somar(3)(4)(5)
console.log("Desafio 1 - Somar: " + total)

const somarAB = somar(4)(2)
console.log("SomarAB: " + somarAB(9))



// 2) calcular(3)(7)(fn) //fn vai calcular 3 e 7
// fn -> 3 * 7
// fn -> 3 + 7
// fn -> 3 - 7

const soma = function (a = 0, b = 0) {
  return "Soma: " + a.toString() + ' + ' + b.toString() + ' = ' + (a + b)
}
const subt = function (a = 0, b = 0) {
  return "Subt: " + a.toString() + ' - ' + b.toString() + ' = ' + (a - b)
}
const mult = function (a = 0, b = 0) {
  return "Mult: " + a.toString() + ' * ' + b.toString() + ' = ' + (a * b)
}

function calcular(a) {
  return function(b) {
    return function(fn) {
      return "Desafio 2 - " + fn(a, b)
    }
  }
}

const r1 = calcular(3)(4)(soma)
const r2 = calcular(5)(3)(subt)
const r3 = calcular(3)(5)(mult)

console.log(r1)
console.log(r2)
console.log(r3)
