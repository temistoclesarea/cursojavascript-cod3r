
const nums = [1, 2, 3, 4, 5]
// map recebe uma função com até 3 valores (n = valor, i = indice, a = array)
// dentro do map tem um laço for interno
const dobro = (n, i, a) => n * 2 + i + a.length

console.log(nums.map(dobro))


const nomes = ['Ana', 'Bia', 'Gui', 'Lia', 'Rafa']

// da pra acessar a primeira letra pelo [] em uma string
const primeiraLetra = texto => texto[0]
const letras = nomes.map(primeiraLetra)

console.log(nomes, letras)

const carrinho = [
  { nome: 'Caneta', qtde: 10, preco: 7.99 },
  { nome: 'Impressora', qtde: 0, preco: 649.50 },
  { nome: 'Caderno', qtde: 4, preco: 27.10 },
  { nome: 'Lapis', qtde: 3, preco: 5.82 },
  { nome: 'Tesoura', qtde: 1, preco: 19.20 },
]

const getNome = item => item.nome
const getTotal = item => item.qtde * item.preco

console.log(carrinho.map(getNome))

const totais = carrinho.map(getTotal)
console.log(totais)

