const fs = require('fs')
const path = require('path')

const caminho = path.join(__dirname, '16-dados.txt')

// pode ser utilizado o _ caso não necessite de um determinado paramentro - uma convenção
function exibirConteudo(_, conteudo) {
  // console.log(conteudo) // mostra o buffer
  console.log(conteudo.toString())
}

console.log('Inicio...')
// javascript permite suprimir a ordem dos parametros sem perder o sentido dos proximos que forem passados
// readFile é assincrono - ele executa apos todo o resto do codigo terminar execução
// fs.readFile(caminho, {}, exibirConteudo)
fs.readFile(caminho, exibirConteudo) // parametro suprimido
fs.readFile(caminho, (_, conteudo) => console.log(conteudo.toString())) // chamada por arrow function
console.log('Fim...')

console.log('Inicio Sync...')
// readFileSync é sincrono ou seja, ele espera para continuar a execução
const conteudo = fs.readFileSync(caminho)
console.log(conteudo.toString())
console.log('Fim Sync...')
